package javapb;

import java.time.LocalDate;

public class Employee {
    private final int id;
    private final String name;
    private final String email;
    private final LocalDate employmentDate;
    private final LocalDate terminationDate;
    private final String status;

    public Employee(int id, String name, String email, LocalDate employmentDate, LocalDate terminationDate, String status) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.employmentDate = employmentDate;
        this.terminationDate = terminationDate;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public LocalDate getEmploymentDate() {
        return employmentDate;
    }

    public LocalDate getTerminationDate() {
        return terminationDate;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return String.format("%-10d%-20s%-25s%s", id, name, email, status);
    }
}
