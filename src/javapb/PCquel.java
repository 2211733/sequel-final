package javapb;

import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class PCquel {
    private static final Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        DB.setConnection();

        while (true) {
            String authChoice = authMenu();
            if (authChoice.equals("1")) {
                createUser();
            } else if (authChoice.equals("2")) {
                try {
                    int customerID = login();
                    Customer customer = DB.getCustomer(customerID);

                    System.out.printf("Welcome to PCquel Repair Shop, %s!%n", customer.getName());
                    boolean inMenu = true;
                    while (inMenu) {
                        int menuChoice = menuSelection();
                        switch (menuChoice) {
                            case 1 -> displayOrders(customerID);
                            case 2 -> requestServiceOrder(customerID, serviceSelection());
                            case 3 -> cancelServiceOrder(customerID);
                            case 4 -> inMenu = false;
                            case 5 -> {
                                DB.closeConnection();
                                scanner.close();
                                System.exit(0);
                            }
                            default -> System.out.println("Choice out of bounds!");
                        }
                    }
                    System.out.println();
                } catch (NumberFormatException e) {
                    System.out.println("Invalid value!");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Your choice is not on the menu!");
            }
        }
    }

    public static String authMenu() {
        System.out.println("1. Register an account");
        System.out.println("2. Login");
        System.out.print("Enter your choice: ");
        return scanner.nextLine();
    }

    public static void createUser() {
        System.out.println("Register an PCquel Account");
        System.out.print("Enter your full name: ");
        String name = scanner.nextLine();
        System.out.print("Enter your phone number: ");
        String phoneNumber = scanner.nextLine();
        System.out.print("Enter your email address: ");
        String email = scanner.nextLine();
        System.out.print("Enter your home address: ");
        String address = scanner.nextLine();
        int id = DB.createCustomer(name, phoneNumber, email, address);
        if (id != 0) {
            System.out.println("Created your account successfully! Your login ID is " + id);
        } else {
            System.out.println("Error creating your account!");
        }
    }

    public static int login() {
        System.out.print("Enter your customer ID: ");
        return Integer.parseInt(scanner.nextLine());
    }

    public static int menuSelection() {
        System.out.println("Enter a menu option");
        System.out.println("1. View your service orders");
        System.out.println("2. Request a service order");
        System.out.println("3. Cancel a service order");
        System.out.println("4. Logout");
        System.out.println("5. Exit");
        System.out.print("Menu option: ");
        return Integer.parseInt(scanner.nextLine());
    }

    public static void displayOrders(int customerId) throws SQLException {
        ArrayList<ServiceOrder> orders = (ArrayList<ServiceOrder>) DB.getOrders(customerId);
        ArrayList<Employee> employees = (ArrayList<Employee>) DB.getEmployees();
        System.out.printf("%-20s%-50s%-20s%-25s%-25s%-10s%-19s%s%n", "Service Order ID", "Service", "Employee Assigned", "Requested On", "Completed On", "Duration", "Total Cost", "Status" );
        for (ServiceOrder so : orders) {
            String serviceName = DB.getServiceName(so.getId());
            Employee employeeAssigned = employees.stream().filter(e -> so.getEmployeeId() == e.getId()).findFirst().orElse(null);
            assert employeeAssigned != null;
            System.out.printf("%-20d%-50s%-20s%-25s%-25s%-10sPHP %-15.2f%s%n",
                    so.getId(),
                    serviceName,
                    employeeAssigned.getName(),
                    so.getRequestedOn() == null ? null : so.getRequestedOn().format(DateTimeFormatter.ofPattern("MM/dd/uuuu HH:mm:ss")),
                    so.getCompletedOn() == null ? null : so.getCompletedOn().format(DateTimeFormatter.ofPattern("MM/dd/uuuu HH:mm:ss")),
                    so.getDuration(),
                    so.getTotalCost(),
                    so.getStatus()
            );
        }
    }

    public static int serviceSelection() throws SQLException {
        ArrayList<Service> services = (ArrayList<Service>) DB.getServices();
        int serviceIndex = 0;
        System.out.printf("    %-50s%-20s%s%n", "Service Name", "Availability", "Cost per Hour");
        for (Service s : services) {
            serviceIndex++;
            System.out.printf("%d.  %s%n", serviceIndex, s);
        }
        System.out.print("Enter service number: ");
        int index = Integer.parseInt(scanner.nextLine());
        if (services.get(index - 1).getAvailability().equals("Unavailable")) {
            return 0;
        }
        return services.get(index - 1).getId();
    }
    public static void requestServiceOrder(int custId, int servId) throws SQLException {
        if (servId == 0) {
            System.out.println("Service is unavailable!");
        } else {
            DB.makeOrder(custId, servId);
            System.out.println("Service order requested!");
        }
    }
    public static void cancelServiceOrder(int custId) throws SQLException {
        ArrayList<ServiceOrder> orders = (ArrayList<ServiceOrder>) DB.getOrders(custId);
        ArrayList<ServiceOrder> ordersInProgress = new ArrayList<>();
        System.out.printf("   %-50s%-25s%s%n", "Service", "Requested On", "Status" );
        int orderIndex = 0;
        for (ServiceOrder so : orders) {
            if (so.getStatus().equals("In progress")) {
                orderIndex++;
                String serviceName = DB.getServiceName(so.getId());
                ordersInProgress.add(so);
                System.out.printf("%d. %-50s%-25s%s%n", orderIndex, serviceName, so.getRequestedOn(), so.getStatus());
            }
        }
        System.out.print("Enter service order to cancel: ");
        int indexOfOrderToCancel = Integer.parseInt(scanner.nextLine());
        ServiceOrder soToCancel = ordersInProgress.get(indexOfOrderToCancel - 1);
        DB.cancelOrder(custId, soToCancel.getId());
        System.out.printf("Order #%d has been cancelled!%n", soToCancel.getId());
    }
}
