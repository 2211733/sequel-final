package javapb;

public class Service {
    private int id;
    private String name;
    private String availability;
    private double costPerHour;

    public Service(int id, String name, String availability, double costPerHour) {
        this.id = id;
        this.name = name;
        this.availability = availability;
        this.costPerHour = costPerHour;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAvailability() {
        return availability;
    }

    public double getCostPerHour() {
        return costPerHour;
    }

    @Override
    public String toString() {
        return String.format("%-50s%-20s%-20.2f", name, availability, costPerHour);
    }
}
