package javapb;

import java.time.LocalDate;

public class Customer {
    private int id;
    private String name;
    private String email;
    private String phoneNumber;
    private String address;
    private LocalDate dateCreated;
    private String status;

    public Customer(int id, String name, String email, String phoneNumber, String address, LocalDate dateCreated, String status) {
        this.id = id;
        this.name = name;
        this.email = email;
        this. phoneNumber = phoneNumber;
        this.address = address;
        this.dateCreated = dateCreated;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return String.format("%-10s%-15s%-25s%-20s%-40s%-15s%s", id, name, phoneNumber, email, address, dateCreated, status);
    }
}