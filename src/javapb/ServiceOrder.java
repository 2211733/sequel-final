package javapb;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ServiceOrder {
    private final int id;
    private final int customerId;
    private final int employeeId;
    private final LocalDateTime requestedOn;
    private final LocalDateTime completedOn;
    private final String duration;
    private final double totalCost;
    private final String status;

    public ServiceOrder(int id, int customerId, int employeeId, LocalDateTime requestedOn, LocalDateTime completedOn, String duration, double totalCost, String status) {
        this.id = id;
        this.customerId = customerId;
        this.employeeId = employeeId;
        this.requestedOn = requestedOn;
        this.completedOn = completedOn;
        this.duration = duration;
        this.totalCost = totalCost;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public LocalDateTime getRequestedOn() {
        return requestedOn;
    }

    public LocalDateTime getCompletedOn() {
        return completedOn;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public String getDuration() {
        return duration;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return String.format("%-10d%-10d%-10d%-25s%-25s%-15s%-15.2f%s", id, customerId, employeeId, requestedOn.format(DateTimeFormatter.ofPattern("MM/dd/uuuu hh:mm:ss")), completedOn, duration, totalCost, status);
    }
}
