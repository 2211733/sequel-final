package javapb;

import javax.swing.text.DateFormatter;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DB {
    private static Connection connection;
    public static void setConnection() {
        try{ 
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pcquel", "root", "");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static int createCustomer(String name, String phoneNumber, String email, String address) {
        try {
            String dateToday = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/uuuu"));

            String query = "INSERT INTO customers (customer_name, phone_number, email, address, date_created, status) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, name);
            statement.setString(2, phoneNumber);
            statement.setString(3, email);
            statement.setString(4, address);
            statement.setString(5, dateToday);
            statement.setString(6, "Active");
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.beforeFirst();
            generatedKeys.next();
            return generatedKeys.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return 0;
        }
    }

    public static Customer getCustomer(int id) throws SQLException {
        String sq = "SELECT * FROM customers WHERE customer_id = ?";
        PreparedStatement statement = connection.prepareStatement(sq);
        statement.setInt(1, id);
        ResultSet result = statement.executeQuery();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/uuuu");
        result.next();
        Customer customer = new Customer(
                result.getInt(1),
                result.getString(2),
                result.getString(3),
                result.getString(4),
                result.getString(5),
                LocalDate.parse(result.getString(6), dtf),
                result.getString(7)
        );
        result.close();
        return customer;
    }

    public static List<ServiceOrder> getOrders(int custId) throws SQLException {
        String sq = "SELECT * FROM service_order_details WHERE customer_id = ?";
        PreparedStatement statement = connection.prepareStatement(sq);
        statement.setInt(1, custId);
        ResultSet result = statement.executeQuery();
        List<ServiceOrder> orders = new ArrayList<>();
        while(result.next()) {
            ServiceOrder so = new ServiceOrder(
                    result.getInt(1),
                    result.getInt(2),
                    result.getInt(3),
                    LocalDateTime.parse(result.getString(4).replaceAll(" ", "")),
                    result.getString(5).equals("Null") ? null : LocalDateTime.parse(result.getString(5)),
                    result.getString(6),
                    result.getString(7).equals("Null") ? 0 : Double.parseDouble(result.getString(7)),
                    result.getString(8)
            );
            orders.add(so);
        }
        result.close();
        return orders;
    }

    public static List<Employee> getEmployees() throws SQLException {
        String sq = "SELECT * FROM employees";
        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet result = statement.executeQuery(sq);
        List<Employee> employees = new ArrayList<>();
        result.beforeFirst();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/uuuu");
        while (result.next()) {
            employees.add(new Employee(
                    result.getInt(1),
                    result.getString(2),
                    result.getString(3),
                    result.getString(4).equals("NULL") ? null : LocalDate.parse(result.getString(4), dtf),
                    result.getString(5).equals("NULL") ? null : LocalDate.parse(result.getString(5), dtf),
                    result.getString(6)
            ));
        }
        return employees;
    }

    public static List<Service> getServices() throws SQLException {
        List<Service> services = new ArrayList<>();
        String sq = "SELECT * FROM services";
        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet result = statement.executeQuery(sq);
        result.beforeFirst();
        while (result.next()) {
            services.add(new Service(
                    result.getInt(1),
                    result.getString(2),
                    result.getString(3),
                    result.getInt(4)
            ));
        }
        result.close();
        return services;
    }

    public static String getServiceName(int id) throws SQLException {
        String query = "SELECT services.service_name FROM services INNER JOIN service_details ON services.service_id = service_details.service_id WHERE service_details.service_order_id = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        ResultSet result = statement.executeQuery();
        result.next();
        return result.getString(1);
    }

    public static void makeOrder(int custId, int serviceId) throws SQLException {
        List<Employee> employees = getEmployees();
        Random random = new Random();
        Employee employee = employees.get(random.nextInt(employees.size()));
        String insertServiceOrder = "INSERT INTO service_order_details (customer_id,employee_id,requested_on,completed_on,duration,total_cost,service_status)" +
                "VALUES(?,?,?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(insertServiceOrder, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, custId);
        statement.setInt(2, employee.getId());
        statement.setString(3, LocalDateTime.now().withNano(0).toString());
        statement.setString(4, "Null");
        statement.setString(5, "Null");
        statement.setString(6, "Null");
        statement.setString(7, "In progress");
        statement.executeUpdate();

        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.beforeFirst();
        generatedKeys.next();
        int key = generatedKeys.getInt(1);

        String insertServiceDetail = "INSERT INTO service_details (service_order_id, service_id, time_started, time_finished, service_duration, total_cost," +
                "pay_status, service_status) VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement statement2 = connection.prepareStatement(insertServiceDetail);
        statement2.setInt(1, key);
        statement2.setInt(2, serviceId);
        statement2.setString(3, "Null");
        statement2.setString(4, "Null");
        statement2.setString(5, "Null");
        statement2.setInt(6, 0);
        statement2.setString(7, "Unpaid");
        statement2.setString(8, "In progress");
        statement2.executeUpdate();
        statement.close();
        statement2.close();
    }

    public static void cancelOrder(int custId, int orderId) throws SQLException {
        String soq = "UPDATE service_order_details SET service_status = 'Cancelled' WHERE service_order_id = ? AND customer_id = ?";
        String sq = "UPDATE service_details SET service_status = 'Cancelled' WHERE service_order_id = ?";
        PreparedStatement soStatement = connection.prepareStatement(soq);
        PreparedStatement sStatement = connection.prepareStatement(sq);

        soStatement.setInt(1, orderId);
        soStatement.setInt(2, custId);

        sStatement.setInt(1, orderId);

        soStatement.executeUpdate();
        sStatement.executeUpdate();
        soStatement.close();
        sStatement.close();
    }
    
    public static void closeConnection() throws SQLException {
    // to close connection
        if (connection != null){
            connection.close();
        }
    }
}
